# *Scelsi Revisited Backstage* Project

These files were part of the presentation held in Darmstadt on August 10 2014
for the *Scelsi Revisited Backstage* project. They concerned an analysis of
Fabien Lévy's work *à tue-tête* (premiered in Darmstadt on August 9 2014)
contrasted - or rather assimilated - with Scelsi's compositions and
compositional methods.

## Compiled version of the slides

A compiled version of the slides may be found over to
[SlideShare](http://www.slideshare.net/NicolaBernardini2/a-tuette-fabien-levys-scelsi-adaption-or-on-the-importance-of-fruitful-contradictions)

## Compilation Instructions

In order to compile the slides you need to also clone the [LaTeX
templates](https://github.com/nicb/latex-templates) at the same level of the
this top root directory.

## Presentation log

* Darmstadt Ferienkursen, Darmstadt August 10 2014

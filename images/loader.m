#
# $Id: loader.m 126 2014-08-13 18:22:58Z nicb $
#
function [frq, semit] = loader(filename)
	data = load(filename);
	X = data(:,1);
	Y = data(:,2);
	[mdb, mdbidx] = max(Y);
	frq = X(mdbidx);
	c4 = 261.6;
	semit = 12*log2(frq/c4);
end

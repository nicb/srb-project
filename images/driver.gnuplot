#
# $Id: driver.gnuplot 126 2014-08-13 18:22:58Z nicb $
#
# gnuplot driver for audacity-produced text files
#
set term svg
set output "@@__FILE__@@.svg"
set logscale x

plot [80:2000] "@@__FILE__@@.txt" w lines lw 4 smooth csplines

#
# $Id: Makefile 126 2014-08-13 18:22:58Z nicb $
#
TOP=..
PRINTMODE=beamer
INCLUDED_MAKE_PATH=$(TOP)/latex-templates/beamer/fis
TARGET=slides.pdf paper.pdf
SOURCE=$(TARGET:.pdf=.tex)
IMAGEDIR=./images
TEXINPUTS=".:$(INCLUDED_MAKE_PATH):"
LATEX=TEXINPUTS=$(TEXINPUTS) pdflatex 
BIBTEX=bibtex8

all: images $(TARGET) 

images:
	$(MAKE) -C $(IMAGEDIR) -$(MAKEFLAGS) 

clean:
	$(RM) *.aux *.log *.out *.toc *.nav *.snm *.dvi *.bbl *.blg $(TARGET)

realclean: clean
	$(MAKE) -C $(IMAGEDIR) -$(MAKEFLAGS) clean

.PHONY: images clean realclean
.PRECIOUS: $(SOURCE)

.SUFFIXES: .pdf .tex

%.pdf: %.tex
	$(LATEX) '\newcommand{\printmode}{$(PRINTMODE)}\input{$<}'
	$(RM) $@
	-$(BIBTEX) $*
	$(LATEX) '\newcommand{\printmode}{$(PRINTMODE)}\input{$<}'
	$(RM) $@
	$(LATEX) '\newcommand{\printmode}{$(PRINTMODE)}\input{$<}'
